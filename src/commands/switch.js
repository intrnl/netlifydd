import { Command } from '@intrnl/clippy';
import kleur from 'kleur';
import { prompt } from '../utils/cli.js';

import { globalConfig } from '../utils/client.js';
import { EnhancedCommand } from '../utils/command.js';


export class SwitchCommand extends EnhancedCommand {
	static paths = [['switch']];

	static usage = Command.Usage({
		description: 'Switch between your currently logged-in Netlify accounts',
	});

	async execute () {
		const currentId = globalConfig.get('active_user');
		const current = currentId && globalConfig.get(`users.${currentId}`);

		const users = globalConfig.get('users');

		const options = [];

		for (const id in users) {
			const user = users[id];

			options.push({
				value: id,
				title: `${user.email}${user.name ? ` (${user.name})` : ''}`,
			});
		}

		if (current) {
			console.log(`Currently logged in as ${kleur.bold(current.email)}${current.name ? ` (${kleur.bold(current.name)})` : ''}`);
		}
		else {
			console.log(`Currently not logged in.`);
		}

		if (options.length < (current ? 2 : 1)) {
			console.log(`There are no other accounts to switch to.`);
			return;
		}

		const next = await prompt({
			type: 'autocomplete',
			message: 'Switch accounts to?',
			choices: options,
		});

		globalConfig.set('active_user', next);
	}
}
