import crypto from 'node:crypto';
import { Command, Option } from '@intrnl/clippy';
import kleur from 'kleur';

import { assertAuthentication, localConfig, request } from '../utils/client.js';
import { AbortError, EnhancedCommand } from '../utils/command.js';
import { promisify, prompt } from '../utils/cli.js';


export class LinkCommand extends EnhancedCommand {
	static paths = [['link']];

	static usage = Command.Usage({
		description: 'Link project folder to a site on Netlify',
	});

	renew = Option.Boolean(['--new']);

	async execute () {
		if (!this.renew && localConfig.get('site_id')) {
			console.log('This project is already linked!');
			return;
		}

		assertAuthentication();

		const option = await prompt({
			type: 'select',
			message: 'How do you want to link this folder?',
			choices: [
				{ value: 'create', title: 'Create a new site' },
				{ value: 'recent', title: 'Choose from a list of your recently updated sites' },
				{ value: 'manual', title: 'Enter a site ID' },
			],
		});

		let siteId;

		switch (option) {
			case 'create': {
				siteId = await this.create();
				break;
			}
			case 'recent': {
				siteId = await this.recent();
				break;
			}
			case 'manual': {
				siteId = await this.manual();
				break;
			}
		}

		localConfig.set('site_id', siteId);
		console.log(`Project is now linked!`);
	}

	async create () {
		const accounts = await promisify({
			message: 'Retrieving list of teams',
			promise: request('/accounts'),
		});

		const account = await prompt({
			type: 'autocomplete',
			message: 'Team',
			choices: accounts.map((account) => ({
				value: account.slug,
				title: account.name,
			}))
		})

		const { adjectives, nouns } = await import('../misc/words.js');

		let site;

		while (true) {
			const randomId = crypto.randomUUID().slice(0, 6);
			const defaultSlug = `${choose(adjectives)}-${choose(nouns)}-${randomId}`;

			const name = await prompt({
				type: 'text',
				message: 'Site name',
				initial: defaultSlug,
				validate: validateSiteName,
			});

			try {
				site = await promisify({
					message: 'Creating site',
					promise: request(`/${account}/sites`, {
						method: 'POST',
						body: {
							name: name,
						},
					}),
				});

				break;
			}
			catch (error) {
				if (error.response?.status === 422) {
					console.error(kleur.red(`> ${kleur.bold(name)}.netlify.app already exists, please try a different name.`));
					continue;
				}

				if (error.response?.status === 429) {
					console.error(kleur.red(`> Too many attempts, please try again.`));
					continue;
				}

				throw error;
			}
		}

		console.log(`> Site created!`);
		return site.id;
	}

	async recent () {
		const list = await promisify({
			message: 'Retrieving list of sites',
			promise: request(`/sites`, {
				params: {
					filter: 'all',
					per_page: 20,
				},
			}),
		});

		if (list.length < 1) {
			console.log(`You don't have any sites yet.`);
			throw new AbortError();
		}

		const site = await prompt({
			type: 'autocomplete',
			message: 'Which site do you want to link?',
			choices: list.map((site) => ({
				value: site,
				name: site.name,
			})),
		});

		return site.id;
	}

	async manual () {
		const siteInput = await prompt({
			type: 'text',
			message: 'Site ID',
			validate: validateSiteName,
		});

		let site;

		try {
			site = await promisify({
				message: 'Checking site',
				promise: request(`/sites/${siteInput}`),
			});
		}
		catch (error) {
			if (error.response?.status === 404) {
				console.error(kleur.red(`> ${kleur.bold(siteInput)} cannot be found.`));
				throw new AbortError();
			}

			throw error;
		}

		return site.id;
	}

}

/**
 * @template Value
 * @param {Value[]} array
 * @returns {Value}
 */
function choose (array) {
	const len = array.length;
	const idx = Math.floor(Math.random() * len);

	const item = array[idx];
	return item;
}

/**
 * @param {string} name
 * @returns {boolean}
 */
function validateSiteName (name) {
	if (name.trim() === '') {
		return 'Site name cannot be empty';
	}

	if ((/[^a-zA-Z0-9-]/).test(name)) {
		return 'Site name can only contain alphanumeric characters and hyphens';
	}

	return true;
}
