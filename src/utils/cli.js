import prompts from 'prompts';
import { assertAbort } from './command.js';
import { createSpinner } from './spinner.js';

/**
 * @template Value
 * @param {object} options
 * @param {Promise<Value>} options.promise
 * @param {string} [options.message]
 * @returns {Promise<Value>}
 */
export function promisify (options) {
	const { message, promise } = options;

	const spinner = createSpinner({ text: message, hideCursor: true });
	spinner.start();

	promise.then(
		() => spinner.stop(),
		() => spinner.stop(),
	);

	return promise;
}

export async function prompt (option) {
	let aborted = false;

	let { result } = await prompts({
		onState: (state) => (aborted = state.aborted || state.exited),
		...option,
		name: 'result',
	});

	assertAbort(!aborted);
	return result;
}
