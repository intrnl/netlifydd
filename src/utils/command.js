import { Command } from '@intrnl/clippy';


export class EnhancedCommand extends Command {
	async catch (error) {
		if (error instanceof AbortError) {
			if (error.message) {
				console.log(message);
			}

			return;
		}

		return super.catch(error);
	}
}

export class AbortError extends Error {}

export function assertAbort (condition, message) {
	if (!condition) {
		throw new AbortError(message);
	}
}
